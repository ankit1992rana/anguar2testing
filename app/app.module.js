"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/* Core Module */
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var http_1 = require('@angular/http');
var forms_1 = require('@angular/forms');
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var router_1 = require('@angular/router');
//import { LocalStorageService, LOCAL_STORAGE_SERVICE_CONFIG } from '@angular/angular-2-local-storage';
/**/
/** Base Container */
var app_base_component_1 = require('./app.base.component');
/* */
/* Pages and directives */
var landing_component_1 = require('./landing/landing.component');
var auth_register_component_1 = require('./auth/auth.register.component');
var auth_login_component_1 = require('./auth/auth.login.component');
var shared_header_component_1 = require('./shared/shared.header.component');
/**/
var localStorageServiceConfig = {
    prefix: 'my-app',
    storageType: 'sessionStorage'
};
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, http_1.HttpModule, forms_1.FormsModule, ng_bootstrap_1.NgbModule.forRoot(),
                router_1.RouterModule.forRoot([
                    { path: '', component: landing_component_1.landingComponent, data: { 'title': 'MindMax' } },
                    { path: 'base', component: app_base_component_1.baseComponent, data: { 'title': 'MindMax' } },
                    { path: 'home', component: shared_header_component_1.headerComponent, data: { 'title': 'Login' } }
                ])
            ],
            declarations: [app_base_component_1.baseComponent, auth_register_component_1.AuthComponent, shared_header_component_1.headerComponent, landing_component_1.landingComponent, auth_login_component_1.loginComponent],
            providers: [http_1.HttpModule,
            ],
            bootstrap: [app_base_component_1.baseComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map