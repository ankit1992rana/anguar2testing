import { inject, TestBed } from '@angular/core/testing';  // Inject and confrigrator
import { BaseRequestOptions, Response, ResponseOptions, Http } from '@angular/http'; 
import { MockBackend, MockConnection } from '@angular/http/testing'; // Dummy server and connection
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { destroyPlatform } from '@angular/core';


import { authService } from './auth.service'
//TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()); // Init but takes too much time

describe('auth Service', () => {

let response = { // A dummy Response
          "statusCode": 200,
          "message": "ok",
          "result": {
            "createdAt": "SOME DATE",
            "updatedAt": "SOME DATE",
            "_id": "123456789",
            "__v": 0,
            "local": {
              "dob": "16/11/2016",
              "name": "Ankit",
              "email": "ankit.rana@daffodilsw.com",
              "following": ["someone"],
              "followers": ["someone"],
              "active": false,
              "deleted": false,
              "rev": 0,
              "ambassdor": false,
              "avatar_url": '',
              "lastlogin_at": "SOME DATE",
              "password": "******"
            }
          },
          "error": false
        }
  //beforeEach(() => destroyPlatform());

  beforeEach(() => {

    // TestBed.initTestEnvironment(
    //   BrowserDynamicTestingModule,
    //   platformBrowserDynamicTesting()
    // );

    TestBed.configureTestingModule({
      providers: [
        authService, // The service
        BaseRequestOptions,
        MockBackend,
        {
          provide: Http, // Use HTTP Module
          useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions) => { // Assigning back and default option, we can also use original server details here
            return new Http(backend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions], // Dependencies
        },
      ],
    });
  });

  beforeEach(inject([MockBackend], (backend: MockBackend) => { // Inject and dummy backend and dummy details
    const baseResponse = new Response(new ResponseOptions({ body: response })); // manually emmitting the response
    backend.connections.subscribe((c: MockConnection) => c.mockRespond(baseResponse)); // Subscribing to the Observable 
    })
  ); // c is for connection

  it('should validate email', inject([authService], (testService: authService) => { // Inject actual service and naming it to testService
    
    testService.validateEmail("ankit.rana@daffodilsw.com").subscribe((res: Response) => { // Calling the function
        //console.log(res);
      expect(res).toBe(response); // The actual Test
    });
  }));
})