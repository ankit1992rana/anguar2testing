"use strict";
var testing_1 = require('@angular/core/testing');
var landing_component_1 = require('./landing.component');
var comp;
var fixture;
var de;
var el;
describe("landingComponent", function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [landing_component_1.landingComponent] // Declare the componenet
        }).compileComponents().then(function () {
            console.log("components are compilied");
        }).catch(function (error) {
            console.log(error);
        });
        fixture = testing_1.TestBed.createComponent(landing_component_1.landingComponent);
        comp = fixture.componentInstance;
    });
    it("A property check", function () {
        fixture.detectChanges();
        expect(comp.title).toBe("mindMax");
    });
    it("A string intropolation text", function () {
        fixture.detectChanges();
        el = fixture.debugElement.nativeElement;
        var title = el.querySelector("h1").innerText;
        expect(title).toBe("mindMax");
    });
});
//# sourceMappingURL=landing.component.spec.js.map