"use strict";
var testing_1 = require('@angular/core/testing'); // Inject and confrigrator
var http_1 = require('@angular/http');
var testing_2 = require('@angular/http/testing'); // Dummy server and connection
var auth_service_1 = require('./auth.service');
//TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()); // Init but takes too much time
describe('auth Service', function () {
    var response = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "createdAt": "SOME DATE",
            "updatedAt": "SOME DATE",
            "_id": "123456789",
            "__v": 0,
            "local": {
                "dob": "16/11/2016",
                "name": "Ankit",
                "email": "ankit.rana@daffodilsw.com",
                "following": ["someone"],
                "followers": ["someone"],
                "active": false,
                "deleted": false,
                "rev": 0,
                "ambassdor": false,
                "avatar_url": '',
                "lastlogin_at": "SOME DATE",
                "password": "******"
            }
        },
        "error": false
    };
    //beforeEach(() => destroyPlatform());
    beforeEach(function () {
        // TestBed.initTestEnvironment(
        //   BrowserDynamicTestingModule,
        //   platformBrowserDynamicTesting()
        // );
        testing_1.TestBed.configureTestingModule({
            providers: [
                auth_service_1.authService,
                http_1.BaseRequestOptions,
                testing_2.MockBackend,
                {
                    provide: http_1.Http,
                    useFactory: function (backend, defaultOptions) {
                        return new http_1.Http(backend, defaultOptions);
                    },
                    deps: [testing_2.MockBackend, http_1.BaseRequestOptions],
                },
            ],
        });
    });
    beforeEach(testing_1.inject([testing_2.MockBackend], function (backend) {
        var baseResponse = new http_1.Response(new http_1.ResponseOptions({ body: response })); // manually emmitting the response
        backend.connections.subscribe(function (c) { return c.mockRespond(baseResponse); }); // Subscribing to the Observable 
    })); // c is for connection
    it('should validate email', testing_1.inject([auth_service_1.authService], function (testService) {
        testService.validateEmail("ankit.rana@daffodilsw.com").subscribe(function (res) {
            //console.log(res);
            expect(res).toBe(response); // The actual Test
        });
    }));
});
//# sourceMappingURL=auth.service.spec.js.map