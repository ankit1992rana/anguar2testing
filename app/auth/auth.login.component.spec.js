"use strict";
var testing_1 = require('@angular/core/testing'); // Inject and confrigrator
var Rx_1 = require('rxjs/Rx');
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
var auth_login_component_1 = require('./auth.login.component');
var auth_service_1 = require('./auth.service');
var comp;
var fixture;
var de;
var el;
var injectedService;
var MockAuthService = (function () {
    function MockAuthService() {
    }
    MockAuthService.prototype.loginUser = function (User) {
        return Rx_1.Observable.of(User).map(//Return a dummy Observable 
        function () {
            return User.toString();
        });
    };
    return MockAuthService;
}());
describe('login Component', function () {
    //  beforeEach(() => destroyPlatform());
    beforeEach(function () {
        // TestBed.initTestEnvironment(
        // BrowserDynamicTestingModule,
        // platformBrowserDynamicTesting()
        // );
        testing_1.TestBed.configureTestingModule({
            declarations: [
                auth_login_component_1.loginComponent
            ]
        });
        fixture = testing_1.TestBed
            .overrideComponent(auth_login_component_1.loginComponent, {
            set: {
                providers: [{ provide: auth_service_1.authService, useClass: MockAuthService }],
            },
        })
            .createComponent(auth_login_component_1.loginComponent); // Creating component
    });
    it('Should work defined', testing_1.fakeAsync(function () {
        //injectedService = fixture.debugElement.injector.get(authService);
        comp = fixture.componentInstance;
        fixture.detectChanges();
        testing_1.tick(); // wait for async function
        fixture.detectChanges(); // update changes 
        expect(comp.doLogin()).toBeDefined; // Function should be defined
        //console.log(comp.doLogin());
    }));
});
//# sourceMappingURL=auth.login.component.spec.js.map