import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: 'header',
    templateUrl: 'app/shared/header.html' 
})
export class headerComponent {
    public title = "mindMax";
    
    constructor(public modalService: NgbModal){};
    
      open(content:any) {
           this.modalService.open(content);
        }
 }
