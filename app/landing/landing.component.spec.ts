import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { landingComponent } from './landing.component';

let comp:    landingComponent;
let fixture: ComponentFixture<landingComponent>;
let de:      DebugElement;
let el:      HTMLElement;

describe("landingComponent", () => {

    beforeEach(()=>{
        

        TestBed.configureTestingModule({
            declarations: [landingComponent] // Declare the componenet
        }).compileComponents().then(function(){
            console.log("components are compilied");
        }).catch(function(error){
            console.log(error);
        });
          fixture = TestBed.createComponent(landingComponent);
                comp = fixture.componentInstance; 

         })

          it("A property check", () => {
                fixture.detectChanges();
                expect(comp.title).toBe("mindMax");
            });

            it("A string intropolation text", () => {
                fixture.detectChanges();
                el = fixture.debugElement.nativeElement;
                let title  = el.querySelector("h1").innerText;
                expect(title).toBe("mindMax");
            });


    
     
    
}) 