/* Core Module */
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import  {Route, Router, RouterLink, RouterModule, RouterOutlet} from '@angular/router';
//import { LocalStorageService, LOCAL_STORAGE_SERVICE_CONFIG } from '@angular/angular-2-local-storage';
/**/

/** Base Container */
import { baseComponent }  from './app.base.component';
/* */

/* Pages and directives */
import { landingComponent } from './landing/landing.component';
import { AuthComponent }  from './auth/auth.register.component';
import { loginComponent } from './auth/auth.login.component';
import { headerComponent } from './shared/shared.header.component';

/**/
let localStorageServiceConfig = {
    prefix: 'my-app',
    storageType: 'sessionStorage'
};


@NgModule({
  
  imports:      [ BrowserModule, HttpModule, FormsModule,NgbModule.forRoot(), 
                  RouterModule.forRoot([
                    { path: '', component: landingComponent, data : {'title' : 'MindMax'} },
                    { path: 'base', component: baseComponent, data : {'title' : 'MindMax'} }, // Just a container to load Views
                    { path: 'home', component: headerComponent, data: {'title' : 'Login'} }
                  ])    
               ],
  declarations: [baseComponent, AuthComponent, headerComponent, landingComponent, loginComponent],
  providers :   [HttpModule, 
                  // LocalStorageService,
                  //   {
                  //       provide: LOCAL_STORAGE_SERVICE_CONFIG, useValue: localStorageServiceConfig
                  //   }
                    ],
  bootstrap:    [baseComponent]
})
export class AppModule { }
