import { inject, TestBed, async, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';  // Inject and confrigrator
//import { BaseRequestOptions, Response, ResponseOptions, Http } from '@angular/http';
//import { MockBackend, MockConnection } from '@angular/http/testing'; // Dummy server and connection
import { destroyPlatform, DebugElement, EventEmitter } from '@angular/core';
import { Subject, Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { loginComponent } from './auth.login.component';
import { authService } from './auth.service';

let comp: loginComponent;
let fixture: ComponentFixture<loginComponent>;
let de: DebugElement;
let el: HTMLElement;
let injectedService: any;

class MockAuthService {
  
    public loginUser(User: Object){

     return Observable.of(User).map( //Return a dummy Observable 
            () => {
              return  User.toString();
            }
           );
    }
}
describe('login Component', () => {

    //  beforeEach(() => destroyPlatform());

    beforeEach(() => { 
        // TestBed.initTestEnvironment(
        // BrowserDynamicTestingModule,
        // platformBrowserDynamicTesting()
        // );


        TestBed.configureTestingModule({ // Configure module
            declarations: [
                loginComponent
            ]
        });
        fixture = TestBed 
                .overrideComponent(loginComponent, { // Overriding component, so that it uses our mock service
                    set: {
                    providers: [{ provide: authService, useClass: MockAuthService }],
                    }, // Using mock service
                })
                .createComponent(loginComponent); // Creating component
            });
   
    it('Should work defined', fakeAsync(() => { //A fake async 
        //injectedService = fixture.debugElement.injector.get(authService);
         comp = fixture.componentInstance;
         fixture.detectChanges();
         tick();                  // wait for async function
         fixture.detectChanges(); // update changes 
         expect(comp.doLogin()).toBeDefined; // Function should be defined
         //console.log(comp.doLogin());
    }));
});