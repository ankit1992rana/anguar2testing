/** Core Services */
import { Component, OnInit } from '@angular/core';
import {Observable, Subject} from 'rxjs/Rx';
//import { LocalStorageService } from 'angular-2-local-storage';


/** User defined Services */
import { authService } from './auth.service'; 

/** Interfaces  */
import { userInit } from './interfaces/loginUser.interface';

@Component({
    selector: 'login',
   // templateUrl: `app/auth/login.html`,
    providers : [authService],
    styleUrls : [],
    template : ''
})

export class loginComponent{
    
    private user = new userInit().user;
    constructor(public _authService: authService){};

    doLogin(){
         this._authService.loginUser(this.user)
        .subscribe(
            data => {
                 data;
                //localStorage.setItem("userName", this.user.email)
            },
            Error => console.log(Error),
            () => { return "done"; }     
        );           
    }
}
