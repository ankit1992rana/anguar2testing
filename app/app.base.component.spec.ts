import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

import { baseComponent } from './app.base.component';
//TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());

describe('AppComponent', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [baseComponent],
      imports: [RouterTestingModule]
    });
  });

  it('routers ahould be working',() => {
    const fixture = TestBed.createComponent(baseComponent);
    fixture.whenStable().then(() => {
      expect(true).toBe(true);
    });
  });

});

