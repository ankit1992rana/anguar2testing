"use strict";
var testing_1 = require('@angular/core/testing');
var testing_2 = require('@angular/router/testing');
var app_base_component_1 = require('./app.base.component');
//TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
describe('AppComponent', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [app_base_component_1.baseComponent],
            imports: [testing_2.RouterTestingModule]
        });
    });
    it('routers ahould be working', function () {
        var fixture = testing_1.TestBed.createComponent(app_base_component_1.baseComponent);
        fixture.whenStable().then(function () {
            expect(true).toBe(true);
        });
    });
});
//# sourceMappingURL=app.base.component.spec.js.map